package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Created by chw4 on 4/04/2017.
 */
public class DynamicRectangleShape extends Shape {

    public DynamicRectangleShape(){
        super();
    }

    public DynamicRectangleShape(int x, int y, int deltaX,int deltaY){
        super(x,y,deltaX,deltaY);
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    @Override
    public void paint(Painter painter) {


        if (typeOfRectagle == 1){
            painter.setColor(Color.black);
            painter.drawRect(fX,fY,fWidth,fHeight);
        }else if(typeOfRectagle == 2){
            painter.setColor(Color.red);
            painter.fillRect(fX,fY,fWidth,fHeight);
        }


    }
}

package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Created by chw4 on 4/04/2017.
 */
public class GemShape extends Shape {

    public GemShape() {
        super();
    }


    public GemShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }


    @Override
    public void paint(Painter painter) {

        if (fWidth > 40) {
            painter.drawPolygon(new Polygon(new int[]{fX+0, fX+20, fX+fWidth - 20, fX+fWidth, fX+fWidth - 20, fX+20}, new int[]{fY+fHeight / 2, fY+fHeight, fY+fHeight, fY+fHeight / 2, fY+0, fY+0}, 6));
        } else if (fWidth <= 40) {

            painter.drawPolygon(new Polygon(new int[]{fX+0, fX+fWidth / 2, fX+fWidth, fX+fWidth / 2}, new int[]{fY+fHeight / 2, fY+fHeight, fY+fHeight / 2, fY+0}, 4));
        }

    }
}
